export class Employee {
   name: string;
   email: string;
   designation: string;
   number: number;
}