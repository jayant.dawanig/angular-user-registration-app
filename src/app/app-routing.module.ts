import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeeCreateComponent } from './components/employee-create/employee-create.component';
import { EmployeeListComponent } from './components/employee-list/employee-list.component';
import { EmployeeEditComponent } from './components/employee-edit/employee-edit.component';
import { EmployeeSignupComponent } from './components/employee-signup/employee-signup.component';
import { EmployeeSigninComponent } from './components/employee-signin/employee-signin.component';
import { EmployeeProfileComponent } from './components/employee-profile/employee-profile.component';

import { EmailVerificationComponent } from './components/email-verification/email-verification.component';
const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'employee-signin' },
  { path: 'create-employee', component: EmployeeCreateComponent },
  { path: 'edit-employee/:id', component: EmployeeEditComponent },
  { path: 'employees-list', component: EmployeeListComponent },
  { path: 'employee-signup', component: EmployeeSignupComponent },
  { path: 'employee-signin', component: EmployeeSigninComponent },
  { path: 'profile-employee/:id', component: EmployeeProfileComponent },
  { path: 'email-verification', component: EmailVerificationComponent },
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }