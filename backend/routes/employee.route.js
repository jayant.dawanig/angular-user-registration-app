
const express = require('express');
const app = express();
const employeeRoute = express.Router();
const bcrypt = require('bcrypt');
var bodyParser = require('body-parser');
const nodemailer = require("nodemailer");
//const { SMTPServer } = require('smtp-server');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
//var bcrypt = require('bcryptjs')
// Employee model
let Employee = require('../models/Employee');
// const SMTPServerInstance = new SMTPServer({
//   // logger: true,
//   // debug: true,
//   authOptional: true
// });
// Add Employee


employeeRoute.route('/create').post((req, res, next) => {
  //console.log(req.body);
  const salt =  bcrypt.genSaltSync(10);
  const hashedPassword =  bcrypt.hashSync(req.body.password,salt);
  req.body.password = hashedPassword;
  Employee.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      
      console.log(req.body.password);
      res.json(data)
     
    }
  })
});

// Get employee login 
employeeRoute.route('/login').post((req, res ) => {


 
   const email = req.body.email;
   //console.log(hashedPassword);
   console.log(email);
   Employee.find({email: email},(error, data)=>{
    if (data[0] == null) {
      res.send("not found");
      console.log(error);
      console.log(data);
      console.log("error is detected");
    }else {
      
      var hash = data[0].password;
      var check = bcrypt.compareSync(req.body.password, hash);
      if(check == 1){
        console.log("loggin successfully");
        res.json(data[0])
        console.log(data[0]._id)
         //send email

      var useremail = req.body.email;
     
      var statusurl = `http://localhost:4000/api/updatestatus/${data[0]._id} `
      console.log(statusurl)
      var smtpConfig = {
        service: 'gmail',
        port: 587,
        auth: {
          user: 'jayant.dawanig@gmail.com',
          pass: ''//enter your gmail password 
        },
       
      };
      
      var transporter = nodemailer.createTransport(smtpConfig);

      var mailOptions = {
        from: 'jayant.dawanig@gmail.com',
        to: useremail,
        subject: 'Email verification process',
        text :   statusurl ,
        
      };


      transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
          console.log("email send successfully from login ");
        }
      });






      
      }
      else{
        console.log("incorrect password");
      }
      
    }
  });


  
   
  // const salt =  bcrypt.genSaltSync(10);
  // const hashedPassword =  bcrypt.hashSync(req.body.password,salt);
  //  const email = req.body.email;
  //  console.log(hashedPassword);
  //  console.log(email);
  //  Employee.find({email: email , password: hashedPassword},(error, data)=>{
  //   if (data[0] == null) {
  //     res.send("not found");
  //     console.log(error);
  //     console.log("error is detected");
  //   }else {
  //     res.send(data);
  //     console.log("loggin successfully");
  //   }
  // });
   
 });
// Get All Employees
employeeRoute.route('/').get((req, res) => {
  
  Employee.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get single employee
employeeRoute.route('/read/:id').get((req, res) => {
  Employee.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  });
});


  

  // const passwordcheck = Employee.findOne({password : req.body.password});
  // if(!passwordcheck){
  //   res.send('password not found ');
  // }
  // else{
  //   res.send("password found");
  // 

// Update employee
employeeRoute.route('/update/:id').put((req, res, next) => {

  Employee.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('Data updated successfully')
    }
  })
})

//// Update employee status
employeeRoute.route('/updatestatus/:id').get((req, res, next) => {

  req.body.status = "1"
  Employee.findByIdAndUpdate(req.params.id,{
     status : req.body.status 
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.redirect(`http://localhost:4200/profile-employee/${req.params.id}`);
      console.log('status activated')
      console.log(req.body.status)
    }
  })
})

// Delete employee
employeeRoute.route('/delete/:id').delete((req, res, next) => {
  Employee.findOneAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = employeeRoute;