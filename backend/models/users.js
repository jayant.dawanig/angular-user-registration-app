const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let Users = new Schema({
   _id: mongoose.Schema.Types.ObjectId,
   name: {
      type: String, 
   },
   email: {
      type: String
   },
   password: {
      type: String
   },
  
}, {
   collection: 'users'
})

module.exports = mongoose.model('users', Users)